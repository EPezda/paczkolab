## Paczkolab - REST Api  ##

Configure your database in config.php file:

$servername = "localhost"; <br>
$username = "root"; <br>
$password = "yourpassword"; <br>
$baseName = "Paczkolab"; <br>

$dsn = "mysql:host=$servername;dbname=$baseName;charset=utf8";

<h4> App usage </h4>

&middot; adding users, addresses, boxes, sizes and parcels <br>

![Scheme](img/menu.png)


&middot; editing and deleting data using SQL relations 

![Scheme](img/edit.png)


